from rest_framework import routers
from .api import ProductsDataAppViewSet

router = routers.DefaultRouter()
router.register('api/products', ProductsDataAppViewSet)
# router.register('api/orders', OrdersDataAppViewSet)
# router.register('api/orders_items', Orders_Items_DataAppViewSet)

urlpatterns = router.urls
