from rest_framework import serializers
from .models import ProductsData,Orders_Items_Data,OrdersData


class ProductsDataAppSerializer(serializers.ModelSerializer):
    class Meta:
        model=ProductsData
        fields='__all__'


class OrdersDataAppSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrdersData
        fields = '__all__'


class Orders_Items_DataAppSerializer(serializers.ModelSerializer):
    class Meta:
        model = Orders_Items_Data
        fields = '__all__'
