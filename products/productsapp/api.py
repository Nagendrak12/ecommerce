from productsapp.models import ProductsData,OrdersData,Orders_Items_Data
from rest_framework import viewsets, permissions
from .serilizers import ProductsDataAppSerializer,OrdersDataAppSerializer,Orders_Items_DataAppSerializer
from rest_framework.filters import SearchFilter,OrderingFilter
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication
#Rest Viewset


class ProductsDataAppViewSet(viewsets.ModelViewSet):
    queryset = ProductsData.objects.all()
    serializer_class = ProductsDataAppSerializer
    permissions_classes = [IsAuthenticated ]
    authentication_classes = [TokenAuthentication]

    # filter_backends=(SearchFilter,OrderingFilter)
    # search_fields=('Title','Description')
    
class OrdersDataAppViewSet(viewsets.ModelViewSet):
    queryset = OrdersData.objects.all()
    permissions_classes = [
        permissions.AllowAny
    ]
    serializer_class = OrdersDataAppSerializer


class Orders_Items_DataAppViewSet(viewsets.ModelViewSet):
    queryset = Orders_Items_Data.objects.all()
    permissions_classes = [
        permissions.AllowAny
    ]
    serializer_class = Orders_Items_DataAppSerializer

